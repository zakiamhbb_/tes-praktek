#define led 13
#define sensor A0
#define pot A1
void setup() {
  // put your setup code here, to run once:
pinMode(sensor,INPUT);
pinMode(led,OUTPUT);
pinMode(pot,INPUT);

Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
a=analogWrite(pot);
b=map(a,0,1023,0,255);

digitalWrite(led, LOW);

if(analogRead(sensor)>=20){
  analogWrite(led,b);
}
else{
  digitalWrite(led,LOW);
}
}
